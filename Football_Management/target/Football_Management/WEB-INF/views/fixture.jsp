<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
    <title>Fixture Page</title>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc; cellpadding:10;
            cellspacing:10;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333; cellpadding:10;
            cellspacing:10;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0; cellpadding:10;
            cellspacing:10;}
        .tg .tg-4eph{background-color:#f9f9f9}

        a.button {
            background-color: #4CAF50; /* Green */
            border: none;
            color: white;
            padding: 10px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 15px;
            margin: 10px 15px 10px 8px;
        }
        a.btn{
            background-color: #f44336;
            border: none;
            color: white;
            padding: 10px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 15px;
            margin: 10px 15px 10px 8px;
        }
        .table {
            border: 1px;

        }
        .th{ width:10%;
        }
        .value{
            margin: 10px 15px 10px 8px;
        }

    </style>
</head>
<body>
<h1>
    Add a Fixture
</h1>

<c:url var="addAction" value="/fixtue/add" ></c:url>

<form:form action="${addAction}" commandName="fixture">
    <table>
        <c:if test="${!empty fixture.fixtureDate}">
            <tr>
                <td>
                    <form:label path="id">
                        <spring:message text="ID"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="id" readonly="true" size="8"  disabled="true" />
                    <form:hidden path="id" />
                </td>
            </tr>
        </c:if>
        <tr>
            <td>
                <form:label path="team1Score">
                    <spring:message text="Team 1 Score"/>
                </form:label>
            </td>
            <td>
                <form:input path="team1Score" />
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="team2Score">
                    <spring:message text="Team 2 Score"/>
                </form:label>
            </td>
            <td>
                <form:input path="team2Score" />
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="fixtureDate">
                    <spring:message text="Fixture Date"/>
                </form:label>
            </td>
            <td>
                <form:input path="fixtureDate" />
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <c:if test="${!empty fixture.fixtureDate}">
                    <input type="submit"
                           value="<spring:message text="Edit Fixture"/>" />
                </c:if>
                <c:if test="${empty fixture.fixtureDate}">
                    <input type="submit"
                           value="<spring:message text="Add Fixture"/>" />
                </c:if>
            </td>
        </tr>
    </table>
</form:form>
<br>
<h3>Fixtures List</h3>
<c:if test="${!empty listFixtures}">
    <table class="tg">
        <tr>
            <th width="80">ID</th>
            <th width="120">Team 1 Score</th>
            <th width="120">Team 2 Score</th>
            <th width="80">Fixture Date</th>
        </tr>
        <c:forEach items="${listFixtures}" var="fixture">
            <tr>
                <td>${fixture.id}</td>
                <td>${fixture.team1Score}</td>
                <td>${fixture.team2Score}</td>
                <td>${fixture.fixtureDate}</td>
                <td><a href="<c:url value='/fixture/edit/${fixture.id}' />"  class="button">Edit</a>
                    <a href="<c:url value='/fixture/remove/${fixture.id}' />"  class="btn">Delete</a></td>
            </tr>
        </c:forEach>
    </table>
</c:if>
</body>
</html>