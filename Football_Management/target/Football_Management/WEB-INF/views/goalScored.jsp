<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
    <title>Goal Scored Page</title>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc; cellpadding:10;
            cellspacing:10;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333; cellpadding:10;
            cellspacing:10;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0; cellpadding:10;
            cellspacing:10;}
        .tg .tg-4eph{background-color:#f9f9f9}

        a.button {
            background-color: #4CAF50; /* Green */
            border: none;
            color: white;
            padding: 10px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 15px;
            margin: 10px 15px 10px 8px;
        }
        a.btn{
            background-color: #f44336;
            border: none;
            color: white;
            padding: 10px 15px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 15px;
            margin: 10px 15px 10px 8px;
        }
        .table {
            border: 1px;

        }
        .th{ width:10%;
        }
        .value{
            margin: 10px 15px 10px 8px;
        }

    </style>
</head>
<body>
<h1>
    Add a Goal Scored
</h1>

<c:url var="addAction" value="/goalScored/add" ></c:url>

<form:form action="${addAction}" commandName="goalScored">
    <table>
        <%--<c:if test="${!empty goalScored.goalTime}">
            <tr>
                <td>
                    <form:label path="id">
                        <spring:message text="ID"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="id" readonly="true" size="8"  disabled="true" />
                    <form:hidden path="id" />
                </td>
            </tr>
        </c:if>--%>
        <tr>
            <td>
                <form:label path="goalTime">
                    <spring:message text="Goal Time"/>
                </form:label>
            </td>
            <td>
                <form:input path="goalTime" />
            </td>
        </tr>


        <tr>
            <td colspan="2">
                <c:if test="${!empty goalScored.goalTime}">
                    <input type="submit"
                           value="<spring:message text="Edit Goal Scored"/>" />
                </c:if>
                <c:if test="${empty goalScored.goalTime}">
                    <input type="submit"
                           value="<spring:message text="Add Goal Scored"/>" />
                </c:if>
            </td>
        </tr>
    </table>
</form:form>
<br>
<h3>Goals Scored List</h3>
<c:if test="${!empty listGoalsScored}">
    <table class="tg">
        <tr>
            <th width="80">ID</th>
            <th width="120">Goal Time</th>
        </tr>
        <c:forEach items="${listGoalsScored}" var="goalScored">
            <tr>
                <td>${goalScored.id}</td>
                <td>${goalScored.goalTime}</td>
                <td><a href="<c:url value='/goalScored/edit/${goalScored.id}' />"  class="button">Edit</a>
                    <a href="<c:url value='/goalScored/remove/${goalScored.id}' />"  class="btn">Delete</a></td>
            </tr>
        </c:forEach>
    </table>
</c:if>
</body>
</html>