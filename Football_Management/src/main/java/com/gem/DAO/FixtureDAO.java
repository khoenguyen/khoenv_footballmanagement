package com.gem.DAO;

import com.gem.Model.Fixture;

import java.util.List;

/**
 * Created by Admin on 6/10/2017.
 */
public interface FixtureDAO {
    public void addFixture(Fixture f);
    public void updateFixture(Fixture f);
    public List<Fixture> fixturesList();
    public Fixture getFixtureById(int id);
    public void removeFixture(int id);
}
