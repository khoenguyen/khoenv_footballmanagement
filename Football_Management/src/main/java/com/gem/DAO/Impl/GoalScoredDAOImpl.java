package com.gem.DAO.Impl;

import com.gem.DAO.GoalScoredDAO;
import com.gem.Model.GoalScored;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Admin on 6/10/2017.
 */
@Repository
public class GoalScoredDAOImpl implements GoalScoredDAO{
    SessionFactory sessionFactory;
    Session session;
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    public static final Logger logger = LoggerFactory.getLogger(GoalScored.class);

    @Override
    public void addGoalScored(GoalScored goalScored) {
        session = sessionFactory.getCurrentSession();
        session.persist(goalScored);
        logger.info("GoalScored saved successfully, GoalScore Detail = " + goalScored);
    }

    @Override
    public void updateGoalScored(GoalScored goalScored) {
        session = sessionFactory.getCurrentSession();
        session.update(goalScored);
        logger.info("GoalScored updated successfully, GoalScore Detail = " + goalScored);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<GoalScored> goalsScoredList() {
        session = sessionFactory.getCurrentSession();
        List<GoalScored> goalsScoredList = session.createQuery("from GoalScored").list();
        for(GoalScored goalScored: goalsScoredList){
            logger.info("GoalScored List: " + goalScored);
        }
        return goalsScoredList;
    }

    @Override
    public GoalScored getGoalScoredById(int id) {
        session = sessionFactory.getCurrentSession();
        GoalScored goalScored = (GoalScored) session.load(GoalScored.class, new Integer(id));
        logger.info("GoalScored loaded successfully, GoalScored detail = " + goalScored);
        return goalScored;
    }

    @Override
    public void removeGoalScored(int id) {
        session = sessionFactory.getCurrentSession();
        GoalScored goalScored = (GoalScored) session.load(GoalScored.class, new Integer(id));
        if(goalScored != null){
            session.delete(goalScored);
        }
        logger.info("GoalScored removed successfully, GoalScored detail = " + goalScored);
    }
}
