package com.gem.DAO.Impl;

import com.gem.DAO.FixtureDAO;
import com.gem.Model.Fixture;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Admin on 6/10/2017.
 */
@Repository
public class FixtureDAOImpl implements FixtureDAO {
    Session session;
    private static final Logger logger = LoggerFactory.getLogger(Fixture.class);
    SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addFixture(Fixture f) {
        session = sessionFactory.getCurrentSession();
        session.persist(f);
        logger.info("Fixture saved successfully, Fixture Detail = " + f);
    }

    @Override
    public void updateFixture(Fixture f) {
        session = sessionFactory.getCurrentSession();
        session.update(f);
        logger.info("Fixture updated successfully, Fixture Detail = " + f);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Fixture> fixturesList() {
        session = sessionFactory.getCurrentSession();
        List<Fixture> fixturesList = session.createQuery("from Fixture").list();
        for(Fixture f : fixturesList){
            logger.info("Fiture List: " + f);
        }
        return fixturesList;
    }

    @Override
    public Fixture getFixtureById(int id) {
        session = sessionFactory.getCurrentSession();
        Fixture f = (Fixture) session.load(Fixture.class, new Integer(id));
        logger.info("Fixture loaded successfully, Fixture detail = " + f);
        return f;
    }

    @Override
    public void removeFixture(int id) {
        session = sessionFactory.getCurrentSession();
        Fixture f = (Fixture) session.load(Fixture.class, new Integer(id));
        if(f != null){
            session.delete(f);
        }
        logger.info("Fixture removed successfully, Fixture detail = " + f);
    }
}
