package com.gem.Model;

import javax.persistence.*;

/**
 * Created by Admin on 6/10/2017.
 */
@Entity
@Table(name = "goal_scored")
public class GoalScored {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "goal_time")
    private int goalTime;

    @Column(name = "player_id")
    private int playerId;

    @Column(name = "fixture_id")
    private int fixtureId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGoalTime() {
        return goalTime;
    }

    public void setGoalTime(int goalTime) {
        this.goalTime = goalTime;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public int getFixtureId() {
        return fixtureId;
    }

    public void setFixtureId(int fixtureId) {
        this.fixtureId = fixtureId;
    }
}
