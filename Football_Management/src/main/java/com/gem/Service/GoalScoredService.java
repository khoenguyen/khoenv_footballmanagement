package com.gem.Service;

import com.gem.Model.GoalScored;

import java.util.List;

/**
 * Created by Admin on 6/10/2017.
 */
public interface GoalScoredService {
    public void addGoalScored(GoalScored goalScored);
    public void updateGoalScored(GoalScored goalScored);
    public List<GoalScored> goalsScoredList();
    public GoalScored getGoalScoredById(int id);
    public void removeGoalScored(int id);
}
