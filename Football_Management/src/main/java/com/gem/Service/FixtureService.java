package com.gem.Service;

import com.gem.Model.Fixture;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Admin on 6/10/2017.
 */
public interface FixtureService {
    public void addFixture(Fixture f);
    public void updateFixture(Fixture f);
    public List<Fixture> fixturesList();
    public Fixture getFixtureById(int id);
    public void removeFixture(int id);
}
