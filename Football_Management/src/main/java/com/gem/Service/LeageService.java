package com.gem.Service;

import com.gem.Model.Leage;

import java.util.List;

/**
 * Created by Admin on 6/10/2017.
 */
public interface LeageService {
    public void addLeage(Leage l);
    public void updateLeage(Leage l);
    public List<Leage> listLeages();
    public Leage getLeageById(int id);
    public void removeLeage(int id);
}
