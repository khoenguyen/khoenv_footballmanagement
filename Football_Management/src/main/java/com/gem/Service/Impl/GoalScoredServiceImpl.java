package com.gem.Service.Impl;

import com.gem.DAO.GoalScoredDAO;
import com.gem.Model.GoalScored;
import com.gem.Service.GoalScoredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Admin on 6/10/2017.
 */
@Service
public class GoalScoredServiceImpl implements GoalScoredService {
    @Autowired
    GoalScoredDAO goalScoredDAO;

    public void setGoalScoredDAO(GoalScoredDAO goalScoredDAO) {
        this.goalScoredDAO = goalScoredDAO;
    }

    @Override
    @Transactional
    public void addGoalScored(GoalScored goalScored) {
        this.goalScoredDAO.addGoalScored(goalScored);
    }

    @Override
    @Transactional
    public void updateGoalScored(GoalScored goalScored) {
        this.goalScoredDAO.updateGoalScored(goalScored);
    }

    @Override
    @Transactional
    public List<GoalScored> goalsScoredList() {
        return this.goalScoredDAO.goalsScoredList();
    }

    @Override
    @Transactional
    public GoalScored getGoalScoredById(int id) {
        return this.goalScoredDAO.getGoalScoredById(id);
    }

    @Override
    @Transactional
    public void removeGoalScored(int id) {
        this.goalScoredDAO.removeGoalScored(id);
    }
}
