package com.gem.Service.Impl;

import com.gem.DAO.FixtureDAO;
import com.gem.Model.Fixture;
import com.gem.Service.FixtureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Admin on 6/10/2017.
 */
@Service
public class FixtureServiceImpl implements FixtureService{
    @Autowired
    private FixtureDAO fixtureDAO;

    public void setFixtureDAO(FixtureDAO fixtureDAO) {
        this.fixtureDAO = fixtureDAO;
    }

    @Override
    @Transactional
    public void addFixture(Fixture f) {
        this.fixtureDAO.addFixture(f);
    }

    @Override
    @Transactional
    public void updateFixture(Fixture f) {
        this.fixtureDAO.updateFixture(f);
    }

    @Override
    @Transactional
    public List<Fixture> fixturesList() {
        return this.fixtureDAO.fixturesList();
    }

    @Override
    @Transactional
    public Fixture getFixtureById(int id) {
        return this.fixtureDAO.getFixtureById(id);
    }

    @Override
    @Transactional
    public void removeFixture(int id) {
        this.fixtureDAO.removeFixture(id);
    }
}
