package com.gem.Controller;

import com.gem.Model.Fixture;
import com.gem.Service.FixtureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Admin on 6/10/2017.
 */
@Controller
public class FixtureController {
    private FixtureService fixtureService;

    @Autowired(required = true)
    @Qualifier(value = "fixtureService")
    public void setFixtureService(FixtureService fs){
        this.fixtureService = fs;
    }
    @RequestMapping(value = "/fixtures", method = RequestMethod.GET)
    public String listFixtures(Model model) {
        model.addAttribute("fixture", new Fixture());
        model.addAttribute("listFixtures", this.fixtureService.fixturesList());
        return "fixture";
    }

    // For add and update Fixture both
    @RequestMapping(value = "/fixtue/add", method = RequestMethod.POST)
    public String addFixture(@ModelAttribute("fixture") Fixture f) {

        if (f.getId() == 0) {
            // new fixture, add it
            this.fixtureService.addFixture(f);
        } else {
            // existing fixture, call update
            this.fixtureService.updateFixture(f);
        }

        return "redirect:/fixtures";
    }
    @RequestMapping("/fixture/remove/{id}")
    public String removeFixture(@PathVariable("id") int id) {

        this.fixtureService.removeFixture(id);
        return "redirect:/fixtures";
    }

    @RequestMapping("/fixture/edit/{id}")
    public String editFixture(@PathVariable("id") int id, Model model) {
        model.addAttribute("fixture", this.fixtureService.getFixtureById(id));
        model.addAttribute("listFixtures", this.fixtureService.fixturesList());
        return "fixture";
    }
}
