package com.gem.Controller;

import com.gem.Model.GoalScored;
import com.gem.Service.GoalScoredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Admin on 6/10/2017.
 */
@Controller
public class GoalScoredController {
    private GoalScoredService goalScoredService;

    @Autowired
    @Qualifier(value = "goalScoredService")
    public void setGoalScoredService(GoalScoredService goalScoredService){
        this.goalScoredService = goalScoredService;
    }

    @RequestMapping(value = "/goalsScored", method = RequestMethod.GET)
    public String listGoalsScored(Model model){
        model.addAttribute("goalScored", new GoalScored());
        model.addAttribute("listGoalsScored", this.goalScoredService.goalsScoredList());
        return "goalScored";
    }
    // For add and update goalScored both
    @RequestMapping(value = "/goalScored/add", method = RequestMethod.POST)
    public String addGoalScored(@ModelAttribute("goalScored") GoalScored goalScored) {

        if (goalScored.getId() == 0) {
            // new goalScored, add it
            this.goalScoredService.addGoalScored(goalScored);
        } else {
            // existing goalScored, call update
            this.goalScoredService.updateGoalScored(goalScored);
        }

        return "redirect:/goalsScored";
    }
    @RequestMapping("/goalScored/remove/{id}")
    public String removeGoalScored(@PathVariable("id") int id) {

        this.goalScoredService.removeGoalScored(id);
        return "redirect:/goalsScored";
    }

    @RequestMapping("/goalScored/edit/{id}")
    public String editGoalScored(@PathVariable("id") int id, Model model) {
        model.addAttribute("goalScored", this.goalScoredService.getGoalScoredById(id));
        model.addAttribute("listGoalsScored", this.goalScoredService.goalsScoredList());
        return "goalScored";
    }
}
